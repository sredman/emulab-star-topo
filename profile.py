"""
This is an Emulab profile for generating a three-level star topology
"""

import geni.portal as portal
import geni.rspec.pg as pg
import geni.rspec.igext as igext

pc = portal.Context()

pc.defineParameter(name="num_cores",
                   description="Number of core routers",
                   typ=portal.ParameterType.INTEGER,
                   defaultValue=4,
                   )
pc.defineParameter(name="lvl1_fanout",
                   description="Number of level 1 routers connect to each core router",
                   typ=portal.ParameterType.INTEGER,
                   defaultValue=3,
                   )
pc.defineParameter(name="core_name",
                   description="Base name for the core routers",
                   typ=portal.ParameterType.STRING,
                   defaultValue="core",
                   )
pc.defineParameter(name="lvl1_name",
                   description="Base name for the level 1 routers",
                   typ=portal.ParameterType.STRING,
                   defaultValue="lvl1",
                   )
pc.defineParameter(name="core_if_name",
                   description="Base name for interfaces which connect to core routers",
                   typ=portal.ParameterType.STRING,
                   defaultValue="core_if")
pc.defineParameter(name="lvl1_if_name",
                   description="Base name for interfaces which connect to level 1 routers",
                   typ=portal.ParameterType.STRING,
                   defaultValue="lvl1_if")
pc.defineParameter(name="core_net_name",
                   description="Base name for a network which contains only core routers",
                   typ=portal.ParameterType.STRING,
                   defaultValue="lan")
pc.defineParameter(name="lvl1_net_name",
                   description="Base name for a network which connects core and level1 routers",
                   typ=portal.ParameterType.STRING,
                   defaultValue="lan")

params = pc.bindParameters()

image_urn = 'emulab.net'
image_project = 'emulab-ops'
image_os = 'UBUNTU16-64'
image_tag = 'STD'

disk_image = 'urn:publicid:IDN+{urn}+image+{proj}//{os}-{tag}'.format(urn=image_urn, proj=image_project, os=image_os, tag=image_tag)

request = pc.makeRequestRSpec()


# Add the core nodes to the topology
cores = []
for core1_idx in range(params.num_cores):
    core = request.XenVM("{}{}".format(params.core_name, core1_idx))
    core.addNamespace(pg.Namespaces.EMULAB)

    core.ram = 512
    core.cores = 1  # Configure the number of CPU cores assigned to this core router
    core.disk = 8
    core.disk_image = disk_image

    cores.append(core)

# Interconnect all core routers
for core1_idx in range(params.num_cores):
    core1 = cores[core1_idx]
    # The name of the interface on the core2 router which connects to core1
    core2_interface_name = "{}{}".format(params.core_if_name, core1_idx)

    for core2_idx in range(core1_idx + 1, params.num_cores):
        core2 = cores[core2_idx]

        # The name of the interface on the core1 router which connects to core2
        core1_interface_name = "{}{}".format(params.core_if_name, core2_idx)

        core1_interface = core1.addInterface(core1_interface_name)
        core2_interface = core2.addInterface(core2_interface_name)

        link = request.LAN("lan-{}-{}".format(core1.name, core2.name))

        link.addInterface(core1_interface)
        link.addInterface(core2_interface)

# Build level 1 ring routers
lvl1_routers = []
for core_idx in range(len(cores)):
    core = cores[core_idx]
    for lvl1_idx in range(0, params.lvl1_fanout):
        router = request.XenVM("{prefix}-{lvl1}-{core}".format(prefix=params.lvl1_name, lvl1=lvl1_idx, core=core_idx))

        router.ram = 512
        router.cores = 1
        router.disk = 8
        router.disk_image = disk_image

        # Connect this router to its core
        core_interface_name = "{prefix}{number}".format(prefix=params.lvl1_if_name, number=lvl1_idx)
        router_interface_name = "{prefix}{number}".format(prefix=params.core_if_name, number=core_idx)

        core_interface = core.addInterface(core_interface_name)
        router_interface = router.addInterface(router_interface_name)

        link = request.LAN("{prefix}-{lvl1}-{core}".format(prefix=params.lvl1_net_name, lvl1=lvl1_idx, core=core_idx))

        link.addInterface(core_interface)
        link.addInterface(router_interface)


pc.printRequestRSpec(request)